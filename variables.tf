variable "instance-count" {}

variable "sg-cidr" {}

variable "vpc-cidr" {}
variable "subnet-cidr-pub" {
    type = list(string)
}
variable "pub-subnet" {}
variable "subnet-cidr-priv" {
    type = list(string)
}
variable "priv-subnet" {}
variable "route-cidr" {}