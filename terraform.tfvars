vpc-cidr                = "10.0.0.0/16"
subnet-cidr-pub         = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
pub-subnet              = "public-subnet"
subnet-cidr-priv        = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
priv-subnet             = "private-subnet"
route-cidr              = "0.0.0.0/0"
sg-cidr                 = "0.0.0.0/0"
instance-count          = 3

