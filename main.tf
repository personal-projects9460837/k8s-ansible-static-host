provider "aws" {
  region     = "eu-west-2"
  profile    = "default"
}

module "vpc" {
    source = "./modules/vpc"
    vpc-cidr         = var.vpc-cidr
    subnet-cidr-pub  = var.subnet-cidr-pub
    subnet-cidr-priv = var.subnet-cidr-priv
    pub-subnet       = var.pub-subnet
    priv-subnet      = var.priv-subnet
    route-cidr       = var.route-cidr

}

module "security_gp" {
  source  = "./modules/security-group"
  sg-cidr = var.sg-cidr
  vpc_id  = module.vpc.vpc-id
}

module "key_pair" {
  source = "./modules/keypair"
}

module "ansible_server" {
  source           = "./modules/ansible"
  ami_ubuntu       = module.key_pair.ami_ubuntu
  key_pair         = module.key_pair.pub_key
  priv_subnet1     = module.vpc.priv-sub1
  ansible_sg       = module.security_gp.ansible_sg
  priv-key         = module.key_pair.priv_key
  haproxy1-ip      = module.haproxy_server.haproxy_master_ip
  haproxy2-ip      = module.haproxy_server.haproxy_backup_ip
  master1-ip       = module.k8s_server.master_ip[0]
  master2-ip       = module.k8s_server.master_ip[1]
  master3-ip       = module.k8s_server.master_ip[2]
  worker1-ip       = module.k8s_server.worker_ip[0]
  worker2-ip       = module.k8s_server.worker_ip[1]
  worker3-ip       = module.k8s_server.worker_ip[2]
  bastion-host     = module.bastion_server.bastion_ip

}

module "bastion_server" {
  source = "./modules/bastion"
  ami_ubuntu    = module.key_pair.ami_ubuntu
  key_pair      = module.key_pair.pub_key
  priv_key      = module.key_pair.priv_key
  pub_subnet1   = module.vpc.pub-sub-1
  bastion_sg    = module.security_gp.bastion_sg

}

module "k8s_server" {
  source = "./modules/k8s-nodes"
  k8s_sg = module.security_gp.k8s_sg
  ami_ubuntu = module.key_pair.ami_ubuntu
  prvsub-id = module.vpc.prvsub-id
  key_pair = module.key_pair.pub_key
  instance-count = var.instance-count
}

module "jenkins_server" {
  source = "./modules/jenkins"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  priv_subnet1 = module.vpc.priv-sub1
  jenkins_sg = module.security_gp.jenkins_sg
}

module "haproxy_server" {
  source = "./modules/haproxy"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  priv_subnet1 = module.vpc.priv-sub1
  priv_subnet3 = module.vpc.priv-sub3
  haproxy_sg = module.security_gp.haproxy_sg
  master1-ip = module.k8s_server.master_ip[0]
  master2-ip = module.k8s_server.master_ip[1]
  master3-ip = module.k8s_server.master_ip[2]
  master4-ip = module.k8s_server.master_ip[0]
  master5-ip = module.k8s_server.master_ip[1]
  master6-ip = module.k8s_server.master_ip[2]

}

module "grafana_lb" {
  source = "./modules/grafana-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  worker-node_id = module.k8s_server.worker_id

}

module "prometheus_lb" {
  source = "./modules/prometheus-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  worker-node_id = module.k8s_server.worker_id

}

module "jenkins_server_lb" {
  source = "./modules/jenkins-lb"
  lb_sg = module.security_gp.jenkins_lb_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  jenkins_server_id = module.jenkins_server.jenkins_id

}

module "prod_lb" {
  source = "./modules/prod-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  worker-node_id = module.k8s_server.worker_id

}

module "stage_lb" {
  source = "./modules/stage-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  worker-node_id = module.k8s_server.worker_id

}
