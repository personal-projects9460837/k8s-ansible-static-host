# create application load balancer for jenkins server
resource "aws_lb" "jenkins_load_balancer" {
  name               = "jenkins-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.lb_sg]
  subnets            = [var.pub_subnet_1, var.pub_subnet_2]
  enable_deletion_protection = false

  tags   = {
    Name = "jenkins-alb"
  }
}

# create target group for jenkins server
resource "aws_lb_target_group" "jenkins_target_group" {
  name        = "jenkins-tg"
  target_type = "instance"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = var.vpc_id

  health_check {
    enabled             = true
    protocol            = "HTTP"
    port                = 8080
    interval            = 30
    path                = "/login"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  lifecycle {
    create_before_destroy = true
  }
}

# create a listener on port 80 with redirect action
resource "aws_lb_listener" "jenkins_http_listener" {
  load_balancer_arn = aws_lb.jenkins_load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.jenkins_target_group.arn
  }
}

# CREATE TARGET GROUP ATTACHEMENT FOR jenkins server
resource "aws_lb_target_group_attachment" "jenkins_alb_attach" {
  target_group_arn = aws_lb_target_group.jenkins_target_group.arn 
  target_id        = var.jenkins_server_id
  port             = 8080
}