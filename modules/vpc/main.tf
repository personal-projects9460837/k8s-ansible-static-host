resource "aws_vpc" "k8s-vpc" {
  cidr_block           = var.vpc-cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "k8s-vpc"
  }
}

data "aws_availability_zones" "available_zone" {}

resource "aws_subnet" "k8s-pub1" {
  vpc_id     = aws_vpc.k8s-vpc.id
  cidr_block = var.subnet-cidr-pub[0]
  availability_zone = data.aws_availability_zones.available_zone.names[0]
  map_public_ip_on_launch = true


  tags = {
    Name = "${var.pub-subnet}-1"
  }
}

resource "aws_subnet" "k8s-pub2" {
  vpc_id     = aws_vpc.k8s-vpc.id
  cidr_block = var.subnet-cidr-pub[1]
  availability_zone = data.aws_availability_zones.available_zone.names[1]
  map_public_ip_on_launch = true


  tags = {
    Name = "${var.pub-subnet}-2"
  }
}

resource "aws_subnet" "k8s-pub3" {
  vpc_id     = aws_vpc.k8s-vpc.id
  cidr_block = var.subnet-cidr-pub[2]
  availability_zone = data.aws_availability_zones.available_zone.names[2]
  map_public_ip_on_launch = true


  tags = {
    Name = "${var.pub-subnet}-3"
  }
}


resource "aws_subnet" "k8s-priv1" {
  vpc_id     = aws_vpc.k8s-vpc.id
  cidr_block = var.subnet-cidr-priv[0]
  availability_zone = data.aws_availability_zones.available_zone.names[0]

  tags = {
    Name = "${var.priv-subnet}-1"
  }
}

resource "aws_subnet" "k8s-priv2" {
  vpc_id     = aws_vpc.k8s-vpc.id
  cidr_block = var.subnet-cidr-priv[1]
  availability_zone = data.aws_availability_zones.available_zone.names[1]

  tags = {
    Name = "${var.priv-subnet}-2"
  }
}

resource "aws_subnet" "k8s-priv3" {
  vpc_id     = aws_vpc.k8s-vpc.id
  cidr_block = var.subnet-cidr-priv[2]
  availability_zone = data.aws_availability_zones.available_zone.names[2]

  tags = {
    Name = "${var.priv-subnet}-3"
  }
}


resource "aws_internet_gateway" "k8s-igw" {
  vpc_id = aws_vpc.k8s-vpc.id

  tags = {
    Name = "k8s-IGW"
  }
}

resource "aws_eip" "k8s-nat" {
  depends_on = [ aws_internet_gateway.k8s-igw ]
}

resource "aws_nat_gateway" "k8s-nat" {
  allocation_id = aws_eip.k8s-nat.id
  subnet_id     = aws_subnet.k8s-pub1.id 

  tags = {
    Name = "k8s-NAT"
  }
}

resource "aws_route_table" "k8s-ig" {
  vpc_id = aws_vpc.k8s-vpc.id

  route {
    cidr_block = var.route-cidr
    gateway_id = aws_internet_gateway.k8s-igw.id
  }
} 

resource "aws_route_table" "k8s-nt" {
  vpc_id = aws_vpc.k8s-vpc.id

  route {
    cidr_block = var.route-cidr
    nat_gateway_id = aws_nat_gateway.k8s-nat.id
  }
} 

resource "aws_route_table_association" "public-assoc-1" {
  subnet_id      = aws_subnet.k8s-pub1.id
  route_table_id = aws_route_table.k8s-ig.id
}

resource "aws_route_table_association" "public-assoc-2" {
  subnet_id      = aws_subnet.k8s-pub2.id
  route_table_id = aws_route_table.k8s-ig.id
}

resource "aws_route_table_association" "public-assoc-3" {
  subnet_id      = aws_subnet.k8s-pub3.id
  route_table_id = aws_route_table.k8s-ig.id
}

resource "aws_route_table_association" "private-assoc-1" {
  subnet_id      = aws_subnet.k8s-priv1.id
  route_table_id = aws_route_table.k8s-nt.id
}

resource "aws_route_table_association" "private-assoc-2" {
  subnet_id      = aws_subnet.k8s-priv2.id
  route_table_id = aws_route_table.k8s-nt.id
}

resource "aws_route_table_association" "private-assoc-3" {
  subnet_id      = aws_subnet.k8s-priv3.id
  route_table_id = aws_route_table.k8s-nt.id
}

