#!/bin/bash
sudo apt update -y
sudo add-apt-repository ppa:vbernat/haproxy-2.4 -y
sudo apt update
sudo apt install haproxy=2.4.\* -y

sudo bash -c 'echo "
frontend myfrontend
  bind 0.0.0.0:6443
  mode tcp
  option tcplog
  default_backend mybackend

backend mybackend
  mode tcp
  option tcp-check
  balance roundrobin
  default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100

  server master1 ${master4-ip}:6443
  server master2 ${master5-ip}:6443
  server master3 ${master6-ip}:6443" > /etc/haproxy/haproxy.cfg'    

sudo systemctl restart haproxy
