output "prod_dns_name" {
  value = aws_lb.prod_load_balancer.dns_name
}

output "prod_zone_id" {
  value = aws_lb.prod_load_balancer.zone_id
}