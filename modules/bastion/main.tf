resource "aws_instance" "bastion-server" {
  ami                         = var.ami_ubuntu
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  key_name                    = var.key_pair
  subnet_id                   = var.pub_subnet1
  vpc_security_group_ids      = [ var.bastion_sg ]
  user_data                   = templatefile("./modules/bastion/bastion.sh", {
    priv_key = var.priv_key
  })

  tags = {
    Name = "bastion-server"
  }
}