resource "aws_security_group" "jenkins_lb_sg" {
  name = "loadbalancer security group"
  description = "Allow inbound traffic from port 80 and 443"
  vpc_id = var.vpc_id

  ingress {
    description      = "http access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  ingress {
    description      = "https access"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }
  
}

resource "aws_security_group" "bastion-sg" {
  name        = "bastion security group"
  description = "Allow inbound traffic from port 22"
  vpc_id      = var.vpc_id

  ingress {
    description      = "ssh access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  } 

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags = {
    Name = "bastion-sg"
    }
}

resource "aws_security_group" "ansible-sg" {
  name        = "ansible security group"
  description = "Allow inbound traffic from port 22"
  vpc_id      = var.vpc_id
  ingress {
    description      = "ssh access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    security_groups  = [aws_security_group.bastion-sg.id] 
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags = {
    Name = "ansible-sg"
  }
}

resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins security group"
  description = "Allow inbound traffic from port 22, 8080, 443 and port 80"
  vpc_id      = var.vpc_id

  ingress {
    description         = "ssh access"
    from_port           = 22
    to_port             = 22
    protocol            = "tcp"
    security_groups     = [aws_security_group.bastion-sg.id]
  }

  ingress {
    description      = "jenkins port access"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  ingress {
    description         = "http access"
    from_port           = 80
    to_port             = 80
    protocol            = "tcp"
    security_groups     = [aws_security_group.jenkins_lb_sg.id]
  }

  ingress {
    description         = "https access"
    from_port           = 443
    to_port             = 443
    protocol            = "tcp"
    security_groups     = [aws_security_group.jenkins_lb_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags = {
    Name = "jenkins-sg"
  }
}

resource "aws_security_group" "k8s-nodes-sg" {
  name        = "k8s-nodes-sg"
  description = "Allow inbound traffic from port range of 0 to 65535"
  vpc_id      = var.vpc_id
  ingress {
    description      = "allow access between port ranges of 0 and 65535"
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags= {
    Name = "k8s-nodes-sg"
  }
}

resource "aws_security_group" "HAproxy_SG" {
  name        = "HAproxy_SG"
  description = "Allow traffic for HAproxy"
  vpc_id      = var.vpc_id

  ingress {
    description = "Allow ssh access"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.sg-cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.sg-cidr]
  }

  tags = {
    Name = "HAproxy_SG"
  }
}