output "jenkins_lb_sg" {
  value = aws_security_group.jenkins_lb_sg.id
}
output "bastion_sg" {
  value = aws_security_group.bastion-sg.id
}

output "ansible_sg" {
  value = aws_security_group.ansible-sg.id
}

output "jenkins_sg" {
  value = aws_security_group.jenkins-sg.id 
}

output "haproxy_sg" {
  value = aws_security_group.HAproxy_SG.id
}

output "k8s_sg" {
  value = aws_security_group.k8s-nodes-sg.id
}