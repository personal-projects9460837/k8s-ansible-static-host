# create application load balancer for stage
resource "aws_lb" "stage_load_balancer" {
  name               = "stage-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.k8s_sg]
  subnets            = [var.pub_subnet_1, var.pub_subnet_2]
  enable_deletion_protection = false

  tags   = {
    Name = "stage-alb"
  }
}

# create target group for stage
resource "aws_lb_target_group" "stage_target_group" {
  name        = "stage-tg"
  target_type = "instance"
  port        = 30001
  protocol    = "HTTP"
  vpc_id      = var.vpc_id

  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    timeout             = 5
    healthy_threshold   = 3
    unhealthy_threshold = 5
  }

  lifecycle {
    create_before_destroy = true
  }
}

# create a listener on port 80 with redirect action
resource "aws_lb_listener" "stage_http_listener" {
  load_balancer_arn = aws_lb.stage_load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.stage_target_group.arn
  }
}

# CREATE TARGET GROUP ATTACHEMENT FOR WORKER PROD
resource "aws_lb_target_group_attachment" "stage-attachment" {
  target_group_arn =aws_lb_target_group.stage_target_group.arn
  target_id = "${element(split(",", join(",", "${var.worker-node_id}")), count.index)}"
  port = 30001
  count = 3
}