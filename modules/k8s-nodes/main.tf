# Create master node instances
resource "aws_instance" "master-node" {
  ami                         = var.ami_ubuntu
  instance_type               = "t2.medium"
  key_name                    = var.key_pair
  subnet_id                   = element(var.prvsub-id, count.index)
  vpc_security_group_ids      = [var.k8s_sg]
  count                       = var.instance-count
 

  tags = {
    Name = "master-${count.index + 1}"
  }
}

# Create worker node instances
resource "aws_instance" "worker-node" {
  ami                         = var.ami_ubuntu
  instance_type               = "t2.medium"
  key_name                    = var.key_pair
  subnet_id                   = element(var.prvsub-id, count.index)
  vpc_security_group_ids      = [var.k8s_sg]
  count                       = var.instance-count
 

  tags = {
    Name = "worker-${count.index + 1}"
  }
}