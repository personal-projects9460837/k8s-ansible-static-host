resource "aws_instance" "Jenkins_Server" {
  ami                           = var.ami_ubuntu
  instance_type                 = "t2.micro"
  key_name                      = var.key_pair
  subnet_id                     = var.priv_subnet1
  vpc_security_group_ids        = [ var.jenkins_sg ]
  user_data                     = local.jenkins_user_data

  tags = {
    Name = "Jenkins_Server"
  }
}