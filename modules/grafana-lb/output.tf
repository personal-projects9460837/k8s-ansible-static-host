output "grafana_dns_name" {
  value = aws_lb.grafana_load_balancer.dns_name
}

output "grafana_zone_id" {
  value = aws_lb.grafana_load_balancer.zone_id
}

