resource "aws_instance" "ansible_server" {
  ami                         = var.ami_ubuntu
  instance_type               = "t2.micro"
  key_name                    = var.key_pair
  subnet_id                   = var.priv_subnet1
  vpc_security_group_ids      = [ var.ansible_sg ]
  user_data = templatefile("./modules/ansible/ansible.sh", {
    priv-key    = var.priv-key,
    haproxy1-ip = var.haproxy1-ip,
    haproxy2-ip = var.haproxy2-ip,
    master1-ip  = var.master1-ip,
    master2-ip  = var.master2-ip,
    master3-ip  = var.master3-ip,
    worker1-ip  = var.worker1-ip,
    worker2-ip  = var.worker2-ip,
    worker3-ip  = var.worker3-ip
  })

  tags = {
    Name = "Ansible_Server"
  }
}

# create null resource to copy playbook directory into ansible server
resource "null_resource" "copy-playbook" {
  connection {
    type = "ssh"
    host = aws_instance.ansible_server.private_ip
    user = "ubuntu"
    private_key = var.priv-key
    bastion_host = var.bastion-host
    bastion_user = "ubuntu"
    bastion_host_key = var.priv-key

  }
  provisioner "file" {
    source = "./modules/ansible/playbooks"
    destination = "/home/ubuntu/playbooks"
  }

  provisioner "file" {
    source      = "./ansible.cfg"
    destination = "/home/ubuntu/ansible.cfg"
  }

}