#!/bin/bash
sudo hostnamectl set-hostname ansible
sudo apt update
sudo apt-add-repository ppa:ansible/ansible
sudo apt update -y
sudo apt install ansible -y
sudo apt install python3-pip -y
sudo apt install python3-boto3 -y

# Copying private key into ansible Server
echo "${priv-key}" >> /home/ubuntu/.ssh/myKey
sudo chown -R ubuntu:ubuntu /home/ubuntu/.ssh/myKey
sudo chmod 400 /home/ubuntu/.ssh/myKey

# Copying the 1st HAproxy into our ha-ip.yml
sudo echo haproxy_master_ip: "${haproxy1-ip}" >> /home/ubuntu/hap-ip.yaml

# Copying the 2st HAproxy into our ha-ip.yml
sudo echo haproxy_backup_ip: "${haproxy2-ip}" >> /home/ubuntu/hap-ip.yaml

#Creating hosts file
touch /home/ubuntu/hosts
sudo chown -R ubuntu:ubuntu /home/ubuntu/hosts

#Adding target servers ip-address to hosts file
echo "[master_main]" >> /home/ubuntu/hosts
echo "master-1 ansible_host=${master1-ip}" >> /home/ubuntu/hosts
echo "[other_master_node]" >> /home/ubuntu/hosts
echo "master-2 ansible_host=${master2-ip}" >> /home/ubuntu/hosts
echo "master-3 ansible_host=${master3-ip}" >> /home/ubuntu/hosts
echo "[worker]" >> /home/ubuntu/hosts
echo "worker-1 ansible_host=${worker1-ip}" >> /home/ubuntu/hosts
echo "worker-2 ansible_host=${worker2-ip}" >> /home/ubuntu/hosts
echo "worker-3 ansible_host=${worker3-ip}" >> /home/ubuntu/hosts
echo "[mast_haproxy]" >> /home/ubuntu/hosts
echo "haproxy_master ansible_host=${haproxy1-ip}" >> /home/ubuntu/hosts
echo "[backup_haproxy]" >> /home/ubuntu/hosts
echo "haproxy_backup ansible_host=${haproxy2-ip}" >> /home/ubuntu/hosts

# Executing playbooks
sudo su -c "ansible-playbook /home/ubuntu/playbooks/installation.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/MastKeepalived.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/master_main.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/other_master_nodes.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/worker.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/monitoring.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/prod.yaml" ubuntu
sudo su -c "ansible-playbook /home/ubuntu/playbooks/stage.yaml" ubuntu


