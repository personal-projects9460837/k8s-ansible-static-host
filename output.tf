output "Worker_nodes_private_ip" {
  value = module.k8s_server.worker_ip
}

output "Master_nodes_private_ip" {
  value = module.k8s_server.master_ip
}

output "bastion_public_ip" {
  value = module.bastion_server.bastion_ip
}

output "haproxy1-servers-ip" {
  value = module.haproxy_server.haproxy_master_ip
}

output "haproxy2-servers-ip" {
  value = module.haproxy_server.haproxy_backup_ip
}

output "jenkins-servers-ip" {
  value = module.jenkins_server.jenkins_ip
}

output "jenkins-servers-dns" {
  value = module.jenkins_server_lb.jenkins_dns_name
}

output "ansible-servers-ip" {
  value = module.ansible_server.ansible_ip
}

output "stage_lb_dns_name" {
  value = module.stage_lb.stage_dns_name
}

output "prod_lb_dns_name" {
  value = module.prod_lb.prod_dns_name
}

output "grafana_lb_dns_name" {
  value = module.grafana_lb.grafana_dns_name
}

output "prometheus_lb_dns_name" {
  value = module.prometheus_lb.prometheus_dns_name
}